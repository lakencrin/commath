#pragma once

#include <vector>
#include <algorithm>
#include <stdexcept>

template <class T>
struct Matrix
{
    using MatrixList = std::vector<T>;
    using MatrixImpl = std::vector<MatrixList>;

private:
    MatrixImpl m_complex_numbers;

public:
    Matrix() = default;
    Matrix(size_t height, size_t width)
    {
        m_complex_numbers.resize(height);
        for (auto& list : m_complex_numbers)
            list.resize(width);
    }
    Matrix(const MatrixImpl& matrix_impl)
        : m_complex_numbers{ matrix_impl }
    {
        size_t width = 0;
        for (auto& list : m_complex_numbers)
            width = std::max(list.size(), width);
        for (auto& list : m_complex_numbers)
        {
            size_t local_width = list.size();
            for (int i = 0; i < width - local_width; ++i)
                list.emplace_back();
        }
    }
    inline const MatrixList& operator[](size_t row) const { return m_complex_numbers.at(row); }
    inline MatrixList& operator[](size_t row) { return m_complex_numbers.operator[](row); }

    size_t width() const { return m_complex_numbers.front().size(); }
    size_t height() const { return m_complex_numbers.size(); }

    bool checkMultiplication(const Matrix& other) const { return width() == other.height(); }
    bool isDimensionEqual(const Matrix& other) const { return width() == other.width() && height() == other.height(); }

    Matrix operator+(const Matrix& other) const
    {
        if (!isDimensionEqual(other))
            throw std::invalid_argument{ "The operation cannot be performed" };

        Matrix result{ height(), width() };
        for (int i = 0; i < height(); ++i)
            for (int j = 0; j < width(); ++j)
                result[i][j] = (*this)[i][j] + other[i][j];
        return result;
    }
    Matrix operator-(const Matrix& other) const
    {
        if (!isDimensionEqual(other))
            throw std::invalid_argument{ "The operation cannot be performed" };

        Matrix result{ height(), width() };
        for (int i = 0; i < height(); ++i)
            for (int j = 0; j < width(); ++j)
                result[i][j] = (*this)[i][j] - other[i][j];
        return result;
    }
    Matrix operator*(const Matrix& other) const
    {
        if (!checkMultiplication(other))
            throw std::invalid_argument{ "The operation cannot be performed (left.width() != right.height())" };

        Matrix result{ height(), other.width() };
        for (int i = 0; i < result.height(); ++i)
            for (int j = 0; j < result.width(); ++j)
            {
                T temp;
                for (int k = 0; k < width(); ++k)
                    temp += (*this)[i][k] * other[k][j];
                result[i][j] = temp;
            }
        return result;
    }
    Matrix operator*(const T& value)
    {
        Matrix result{ height(), width() };
        for (int i = 0; i < height(); ++i)
            for (int j = 0; j < width(); ++j)
                result[i][j] = (*this)[i][j] * value;
        return result;
    }
};
