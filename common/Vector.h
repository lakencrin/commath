#pragma once

#include <vector>
#include <stdexcept>

template <typename T>
class Vector
{
	T* m_data;
	size_t m_size;
	bool _check_index(size_t index) const { return index >= 0 && index < m_size; }
public:
	Vector()
		: m_data{ nullptr }
		, m_size{ 0 }
	{}

	Vector(size_t size)
		: m_data{ new T[size]{} }
		, m_size{ size }
	{}
	Vector(const std::vector<T>& vector)
		: m_data{ new T[vector.size()]{} }
		, m_size{ vector.size() }
	{
		std::copy(vector.begin(), vector.end(), m_data);
	}
	Vector(const Vector& vector)
		: m_data{ new T[vector.size()]{} }
		, m_size{ vector.size() }
	{
		m_data[0] = vector[0];
		m_data[1] = vector[1];
		m_data[2] = vector[2];
	}
	Vector(Vector&& vector)
		: m_data{ vector.m_data }
		, m_size{ vector.m_size }
	{
		vector.m_data = nullptr;
		vector.m_size = 0;
	}
	Vector& operator=(const Vector& vector)
	{
		if (this == &vector) return *this;
		m_data = new T[vector.size()]{};
		m_size = vector.m_size;
		std::copy(vector.begin(), vector.end(), m_data);
		return *this;
	}
	Vector& operator=(Vector&& vector)
	{
		m_data = vector.m_data;
		m_size = vector.m_size;
		vector.m_data = nullptr;
		vector.m_size = 0;
		return *this;
	}
	~Vector() { delete m_data; }

	T& operator[](size_t index) { return _check_index(index) ? m_data[index] : throw std::exception{}; }
	const T& operator[](size_t index) const { return _check_index(index) ? m_data[index] : throw std::exception{}; }

	template <class V>
	Vector operator*(V value) const
	{
		Vector result{ *this };
		for (int i = 0; i < m_size; ++i)
			result[i] *= value;
		return result;
	}

	size_t size() const { return m_size; }
};


