#pragma once

#include "Vector.h"

#include <iostream>

template<typename T>
std::ostream& operator<<(std::ostream& out, Vector<T>& vector)
{
	for (int i = 0; i < vector.size(); ++i)
		out << vector[i] << ' ';
	out << std::endl;
	return out;
}

template<typename T>
std::istream& operator>>(std::istream& in, Vector<T>& vector)
{
	std::vector<T> result;

	do
	{
		T t;
		in >> t;
		result.push_back(t);
	} while (in.get() != '\n');

	vector = { result };
	return in;
}