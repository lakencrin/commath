#pragma once

#include "Matrix.h"
#include <iostream>

template <class T>
std::ostream& operator<<(std::ostream& out, const Matrix<T>& matrix)
{
    for (int i = 0; i < matrix.height(); ++i)
    {
        const Matrix<T>::MatrixList& list = matrix[i];
        for (auto& value : list)
            std::cout << value << ' ';
        std::cout << std::endl;
    }
    return out;
}

template <class T>
std::istream& operator>>(std::istream& in, Matrix<T>& matrix)
{
    typename Matrix<T>::MatrixImpl complex_numbers;
    do
    {
        typename Matrix<T>::MatrixList list;
        do
        {
            T complex;
            in >> complex;
            list.push_back(complex);
        } while (in.get() != '\n');

        complex_numbers.push_back(std::move(list));
    } while (in.peek() != '\n');
    matrix = Matrix<T>{ complex_numbers };
    return in;
}