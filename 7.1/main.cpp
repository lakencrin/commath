﻿#include <iostream>
#include <vector>

struct Data
{
	double x;
	double y;
};

std::istream& operator>>(std::istream& in, Data& data)
{
	in >> data.x >> data.y;
	return in;
}

double u_cal(double u, int n) {
    double temp = u;
    for (int i = 1; i < n; i++) temp *= (u - i);
    return temp;
}

double fact(int n) {
    double f = 1.;
    for (int i = 2; i <= n; i++) f *= i;
    return f;
}

int main() 
{
	size_t n = 0;
	std::cin >> n;
	std::vector<Data> f(n, Data{});
	for (int i = 0; i < n; ++i)
		std::cin >> f[i];
    //Таблица конечных разностей (вперёд)
	std::vector<std::vector<double>> y{ n, std::vector<double>((const size_t)n, 0.) };
    for (int i = 0; i < n; i++) 
		y[i][0] = f[i].y;
    for (int i = 1; i < n; i++) 
        for (int j = 0; j < n - i; j++) 
			y[j][i] = y[j + 1][i - 1] - y[j][i - 1];

	double const x1 = 0;
	double const x2 = 5;
	double const dx = 0.125;

    std::cout.width(20); std::cout << "X"; std::cout.width(20); std::cout << "Y" << std::endl;
    for (double x = x1; x <= x2; x += dx) {
        double sum = y[0][0];
        double u = (x - f[0].x) / (f[1].x - f[0].x);
        for (int i = 1; i < n; i++) {
            sum += (u_cal(u, i) * y[0][i]) / fact(i);
        }
        std::cout.width(20); std::cout.precision(14); std::cout << x;
        std::cout.width(20); std::cout.precision(14); std::cout << sum << std::endl;
    }

    return 0;
}

/*
Input example:
4
0 2
1 3
2 12
5 147

*/