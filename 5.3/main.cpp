﻿#include "MatrixIn.h"
#include "VectorIn.h"
#include "LUD.h"

#include <iostream>

int main()
{
    DoubleMatrix matrix;
    std::cin >> matrix;
    Vector<double> b;
    std::cin >> b;

    Vector<double> pair = LowerUpperDecomposition::solve(matrix, b);
    std::cout << pair;
    return 0;
}

/*
Input example:

1 3 3
1 -2 3
3 3 -1

11 1 1

*/