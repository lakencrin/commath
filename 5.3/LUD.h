#pragma once

#include "Matrix.h"
#include "Vector.h"

using DoubleMatrix = Matrix<double>;

class LowerUpperDecomposition
{
    using DoubleMatrixPair = std::pair<DoubleMatrix, DoubleMatrix>;

    static DoubleMatrixPair matrices(const DoubleMatrix& matrix)
    {
        DoubleMatrixPair pair{ { matrix.height(), matrix.width() }, { matrix.height(), matrix.width() } };
        const size_t rank = matrix.height();
        DoubleMatrix& b_mat = pair.first;
        DoubleMatrix& c_mat = pair.second;

        for (int i = 0; i < rank; ++i)
            b_mat[i][0] = matrix[i][0];

        for (int j = 0; j < rank; ++j)
        {
            c_mat[0][j] = matrix[0][j] / matrix[0][0];
            c_mat[j][j] = 1;
        }

        for (int i = 1; i < rank; ++i)
        {
            for (int row = i; row < rank; ++row)
            {
                int const col = i;
                b_mat[row][col] = matrix[row][col];
                for (int k = 0; k < i; ++k)
                    b_mat[row][col] -= b_mat[row][k] * c_mat[k][col];
            }
            for (int col = i + 1; col < rank; ++col)
            {
                int const row = i;
                c_mat[row][col] = matrix[row][col];
                for (int k = 0; k < i; ++k)
                    c_mat[row][col] -= b_mat[row][k] * c_mat[k][col];
                c_mat[row][col] /= b_mat[i][i];
            }
        }

        return pair;
    }
public:
    static Vector<double> solve(const DoubleMatrix& matrix, const Vector<double>& d)
    {
        DoubleMatrixPair pair = matrices(matrix);
        Vector<double> y{ d.size() };
        for (size_t i = 0; i < y.size(); ++i)
        {
            double& value = y[i];
            y[i] = d[i];
            for (size_t k = 0; k < i; ++k)
            {
                double temp_b = pair.first[i][k];
                value -= temp_b * y[k];
            }
            y[i] /= pair.first[i][i];
        }

        Vector<double> x{ d.size() };
        for (size_t i = x.size() - 1; i != (size_t)-1; --i)
        {
            x[i] = y[i];
            for (size_t k = i + 1; k < d.size(); ++k)
                x[i] -= pair.second[i][k] * x[k];
        }
        return x;
    }
};
