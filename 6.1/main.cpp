﻿#include "Matrix.h"
#include "MatrixIn.h"
#include "Vector.h"
#include "VectorIn.h"

#include <iostream>
#include <string>

enum class ProductType
{
    Scalar,
    Vector,
};

enum class ElementType
{
    Row,
    Column,
};

Vector<double> scalar_product(const std::vector<double>& left, const std::vector<double>& right);
Vector<double> vector_product(const std::vector<double>& left, const std::vector<double>& right);

int main()
{
	try {

    Matrix<double> matrix;

    std::cout << "Matrix:\n";
    std::cin >> matrix;

    std::cout << "\nEnter command (v|s)(r|c)((<idx>, <idx>)):";
    std::string string;
    std::cin >> string;

    ProductType product_type;
    {
        char const product_type_flag = string.front();
        switch (product_type_flag)
        {
        case 's':
            product_type = ProductType::Scalar;
            break;
        case 'v':
            product_type = ProductType::Vector;
            break;
        default:
            throw std::invalid_argument{ "Invalid ProductType" };
        }
    }
    if (product_type == ProductType::Vector && (matrix.height() != 3 || matrix.width() != 3))
        throw std::invalid_argument{ "Invalid ProductType" };

    ElementType element_type;
    {
        char const element_type_flag = *(string.begin() + 1);
        switch (element_type_flag)
        {
        case 'r':
            element_type = ElementType::Row;
            break;
        case 'c':
            element_type = ElementType::Column;
            break;
        default:
            throw std::invalid_argument{ "Invalid ElementType" };
        }
    }
    string = string.substr(2);
    if (string.front() != '(' || string.back() != ')')
        throw std::invalid_argument{ "Invalid format" };

    string = string.substr(1, string.length() - 2);
    std::string::iterator point = std::find(string.begin(), string.end(), ',');
    std::string first_string = std::string{ string.begin(), point };
    std::string second_string = std::string{ point + 1, string.end() };

    size_t first = std::stol(first_string);
    size_t second = std::stol(second_string);

    Vector<double> result;

    if (element_type == ElementType::Row)
    {
        if (first < 0 || first >= matrix.height() || second < 0 || second >= matrix.height() || first == second)
            throw std::invalid_argument{ "Invalid indices" };

        if (product_type == ProductType::Scalar)
            result = scalar_product(matrix[first], matrix[second]);
        else
            result = vector_product(matrix[first], matrix[second]) * std::pow(-1, first + second);
    }
    else
    {
        if (first < 0 || first >= matrix.width() || second < 0 || second >= matrix.width() || first == second)
            throw std::invalid_argument{ "Invalid indices" };

        std::vector<double> left(matrix.height()), right(matrix.height());

        for (int i = 0; i < matrix.height(); ++i)
        {
            left[i] = matrix[first][i];
            right[i] = matrix[second][i];
        }
        if (product_type == ProductType::Scalar)
            result = scalar_product(left, right);
        else
            result = vector_product(left, right) * std::pow(-1, first + second);
    }

    std::cout << "Result:\n" << result;
	return 0;
	}
	catch (std::exception & e)
	{
		std::cout << "Error: " << e.what();
	}
    return 1;
}

/*
Input example:
1 3 3
1 -2 3
3 3 -1

vr(0,1)
*/


Vector<double> scalar_product(const std::vector<double>& left, const std::vector<double>& right)
{
    if (left.size() != right.size())
        throw std::invalid_argument{ "Dimensions of arguments are not equal" };

    Vector<double> result{ left.size() };
    for (int i = 0; i < left.size(); ++i)
        result[i] = left[i] * right[i];
    return result;
}

Vector<double> vector_product(const std::vector<double>& left, const std::vector<double>& right)
{
    if (left.size() != 3 || right.size() != 3)
        throw std::invalid_argument{ "Only 3-dimensional vectors are allowed to multiply" };

    Vector<double> result{ (size_t)3 };
    result[0] = left[2] * right[1] - left[1] * right[2];
    result[1] = left[0] * right[2] - left[2] * right[0];
    result[2] = left[1] * right[0] - left[0] * right[1];
    return result;
}