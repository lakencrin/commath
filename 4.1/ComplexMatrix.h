#pragma once
#include "Matrix.h"
#include "MatrixIn.h"
#include "Complex.h"
#include "Input.h"

#include <iostream>

using ComplexMatrix = Matrix<Complex>;
using ComplexList = std::vector<Complex>;

std::istream& operator>>(std::istream& in, ComplexMatrix& matrix)
{
    ComplexMatrix::MatrixImpl complex_numbers;
    while (!std::cout.bad())
    {
        std::string string;
        std::getline(in, string);

        if (string.empty())
            break;

        ComplexList list;
        while (!string.empty())
        {
            Complex complex;
            std::string::iterator point = _convert_helper(string, complex);
            list.push_back(complex);
            string.erase(string.begin(), point);
        }
        complex_numbers.push_back(std::move(list));
    }
    size_t width = 0;
    for (auto& list : complex_numbers)
        width = std::max(list.size(), width);
    matrix = ComplexMatrix{ complex_numbers };
    return in;
}