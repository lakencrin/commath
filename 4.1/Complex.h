#pragma once

class Complex
{
    double m_re, m_im;
public:
    constexpr Complex(double re = 0., double im = 0.)
        : m_re{ re }
        , m_im{ im }
    {}
    constexpr Complex(const Complex& other)
        : m_re{ other.m_re }
        , m_im{ other.m_im }
    {}

    constexpr double real() const { return m_re; }
    constexpr double imag() const { return m_im; }

    constexpr Complex operator+(const Complex& right) const
    {
        double const re = real() + right.real();
        double const im = imag() + right.imag();
        return Complex{ re, im };
    }

    constexpr Complex operator-(const Complex& right) const
    {
        double const re = real() - right.real();
        double const im = imag() - right.imag();
        return Complex{ re, im };
    }

    constexpr Complex operator*(const Complex& right) const
    {
        double const re = real() * imag() - right.real() * right.imag();
        double const im = real() * imag() + right.real() * right.imag();
        return Complex{ re, im };
    }

    constexpr Complex operator/(const Complex& right) const
    {
        double const re = (real() * imag() + right.real() * right.imag()) / (imag() * imag() + right.imag() * right.imag());
        double const im = (imag() * right.real() - real() * right.imag()) / (imag() * imag() + right.imag() * right.imag());
        return Complex{ re, im };
    }
    constexpr Complex& operator+=(const Complex& right)
    {
        return *this = (*this + right);
    }
    constexpr Complex operator-=(const Complex& right)
    {
        return *this = (*this - right);
    }
    constexpr Complex operator*=(const Complex& right)
    {
        return *this = (*this * right);
    }
    constexpr Complex operator/=(const Complex& right)
    {
        return *this = (*this / right);
    }
};

