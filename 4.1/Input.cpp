#include "Input.h"
#include <algorithm>

std::istream& operator>>(std::istream& in, Complex& complex)
{
    std::string string;
    in >> string;

    _convert_helper(string, complex);

    return in;
}

std::ostream& operator<<(std::ostream& out, const Complex& complex)
{
    out << complex.real();
    if (complex.imag())
        out << 'i' << complex.imag();
    out << ' ';
    return out;
}

std::string::iterator _convert_helper(std::string& input, Complex& complex)
{
    std::string::iterator point;
    size_t index;
    double const real = std::stod(input, &index);
    point = input.begin() + index;
    double imag = 0;

    auto find_end = std::find_if(input.begin(), input.end(), std::isspace);
    {
        if (std::find(input.begin(), find_end, 'i') != find_end)
        {
            imag = std::stod(std::string{ input.data() + index + 1 }, &index);
            point = find_end;
        }
    }
    complex = Complex{ real, imag };
    return std::find_if(find_end, input.end(), std::isgraph);
}