#pragma once

#include "Complex.h"

#include <iostream>
#include <string>

std::string::iterator _convert_helper(std::string& input, Complex& complex);
std::istream& operator>>(std::istream& in, Complex& complex);
std::ostream& operator<<(std::ostream& out, const Complex& complex);
