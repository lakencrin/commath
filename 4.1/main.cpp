﻿#include "ComplexMatrix.h"

int main()
{
    ComplexMatrix matrix_left, matrix_right;
    std::cin >> matrix_left >> matrix_right;
    std::cout << "Addition\n" << matrix_left + matrix_right;
    std::cout << "\nSubtraction\n" << matrix_left - matrix_right;
    std::cout << "\nMultiplication\n" << matrix_left * matrix_right;
    std::cout << "\nMultiplication by number\n" << matrix_left * Complex{ 5, 10 };
    return 0;
}

/*
Input example:

5i1 4i2
2i0.5 -7i-10000

1 5
1i10.1111 1i10.1111

*/

